Description du composant RpaBundle
==

Introduction
--
Le module RPA (Robotic Process Automation) est une technologie d'automatisation permettant l'acquisition et/ou l'injection d'informations à partir d'applications web. Le robot exécute une séquence de commandes sous un ensemble défini de règles et de méthodologie.

Installation
--

1. Déclaration du package au sein du composer.json

Rajouter le repository externe pour le bundle utils de la manière suivante :

```yaml
"repositories": [
  ...
  {
    "type": "vcs",
    "url": "https://gitlab.adullact.net/friend-of-pastry-garden/component/utilsbundle.git"
  },
  {
    "type": "vcs",
    "url": "https://gitlab.adullact.net/friend-of-pastry-garden/component/rpa-bundle.git"
  }
  ...
]
```

2. Appel du bundle pour votre application

``
composer require friend-of-pastry-garden/rpa-bundle
``

3. Configuration du proxy

Le proxy se configure directement au niveau de l'environnement de la machine. Une fois fait il sera pris en compte automatiquement par le robot.

#### Exemple de déclaration de proxy depuis le Dockerfile

```bash
ENV http_proxy "http://rie-proxy.justice.gouv.fr:8080"
ENV https_proxy "http://rie-proxy.justice.gouv.fr:8080"
ENV ftp_proxy "http://rie-proxy.justice.gouv.fr:8080"
ENV no_proxy ".intranet.justice.gouv.fr,localhost,127.0.0.1,selenium,selenium_firefox"
```

4. Configuration de l'environnement de test

L'environnement de test est fonction de l'écosystème de l'équipe qui doit intégrer l'applicatif. Un ensemble de variable d'environnement doit être définie dans le .env.test.local

| variable |  utilisation | client | exemple |
| ------ | ------ | ------ | ------ |
| TEST__DEFAULT_SELENIUM_CHROME_SERVER_URL | Adresse du serveur SELENIUM. | Selenium | http://selenium:4444/wd/hub |
| TEST__DEFAULT_TAG | Balise HTML du site web utilisé par défaut pour les tests communs. | Goutte, Selenium | h1 |
| TEST__DEFAULT_TAG_VALUE | Texte contenu dans la balise HTML du site web utilisé par défaut pour les tests communs. | Goutte, Selenium | FusionForge de l'ADULLACT: Bienvenue |
| TEST__DEFAULT_WEBSITE | Site web utilisé par défaut pour les tests communs. | Goutte, Selenium | http://github.com |
| TEST__DEFAULT_WEBSITE_WITH_SSO | Site web utilisé passant par une authentification SSO. | Selenium | http://mysite.fr |
| TEST__DEFAULT_SSO_LABEL | Texte du bouton de connexion sur la page SSO | Selenium | Connect |
| TEST__DEFAULT_SSO_PARAMS | Tableau de donnée du formulaire pour la connexion SSO | Selenium | {user: titi, password: toto} |
| TEST__DEFAULT_SSO_URL_REGEXP | Expression régulière permettant de reconnaître l'URL de la page SSO | Selenium | /mysso[.]fr$/i |
| TEST__DEFAULT_SSO_TAG | Balise HTML du site web ou l'on atterit après authentification SSO | Selenium | title |
| TEST__DEFAULT_TAG_VALUE | Texte contenu dans la balise HTML du site web ou l'on atterit après authentification SSO | Selenium | Hello world! |
