<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Symfony\Bundle\MakerBundle\MakerBundle::class => ['dev' => true],
    FOPG\Component\UtilsBundle\FOPGComponentUtilsBundle::class => ['all' => true],
];
