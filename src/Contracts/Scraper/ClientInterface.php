<?php
namespace FOPG\Component\RpaBundle\Contracts\Scraper;

use Symfony\Component\DomCrawler\Crawler as DomCrawler;
use Symfony\Component\DomCrawler\Form as DomForm;

interface ClientInterface
{
  const CLIENT_GOUTTE='goutte';
  const CLIENT_CHROME='chrome';
  const CLIENT_FIREFOX='firefox';
  const CLIENT_SELENIUM_CHROME='selenium_chrome';
  const CLIENT_SELENIUM_FIREFOX='selenium_firefox';

  public function execute(string $url,string $method,array $queryParameters): ?DomCrawler;
  public function getCrawler(): DomCrawler;
  public function quit(): void;
  public function submit(DomForm $domForm,array $data): ?DomCrawler;
  public function workWithJS(): bool;
}
