<?php
namespace FOPG\Component\RpaBundle\Contracts\Scraper;

interface ExceptionInterface
{
  const CODE_NO_SUCH_ELEMENT_FOUND = 410;
  const CODE_AUTH_SSO_FAILED = 411;
  const CODE_NO_SUCH_ELEMENT_FOUND_OR_VISIBLE = 412;
  const CODE_EXECUTE_FAILED = 413;
  const CODE_NO_FORM_FOUND = 414;

  const MESSAGE_AUTH_SSO_FAILED = 'Invalid auth SSO !';
}
