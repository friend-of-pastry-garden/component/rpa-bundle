<?php

namespace FOPG\Component\RpaBundle\Contracts\Scraper;

interface ScraperConstraintInterface
{
  const ACTION_AWAITING       ='awaiting';
  const ACTION_CHECK_CLASS    ='check-class';
  const ACTION_CHECK_SELECTOR ='check-selector';
  const ACTION_CHECK_ID       = 'check-id';
  const STATE_VISIBLE         = 'visible';
  const STATE_PRESENT         = 'present';
  const STATE_NONE            = 'none';
}
