<?php
namespace FOPG\Component\RpaBundle\Contracts\Scraper;

use Symfony\Component\DomCrawler\Crawler as DomCrawler;
use Symfony\Component\HttpFoundation\Request;

interface ScraperInterface
{
  const FORM_LABEL = 'label';
  const FORM_NAME = 'form';
  const FORM_PARAMETERS = 'params';
  const FORM_OPTIONS = 'form-options';
  const FORM_DISABLE_PRE_EXECUTE = 'form-disable-pre-execute';
  const SSO_LABEL = "sso-label";
  const SSO_PARAMS = "sso-parameters";
  const SSO_URL_REGEXP = "sso-url-regexp";
  const STRATEGY_DOM_CRAWLER = 'strategy-dom-crawler';
  const STRATEGY_JS = 'strategy-js';

  /**
   * Execute on network after prepare statement
   *
   * @param string $method
   */
  public function execute(string $method=Request::METHOD_GET): ?DomCrawler;

  /**
   * Execute on network by submit form
   *
   * @param array $form
   */
  public function submit(array $form=[]): ?DomCrawler;
}
