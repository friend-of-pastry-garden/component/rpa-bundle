<?php
namespace FOPG\Component\RpaBundle\Exception\Scraper;

use FOPG\Component\RpaBundle\Contracts\Scraper\ExceptionInterface;

class AuthSsoFailedException extends \Exception implements ExceptionInterface
{
	public function __construct()
	{
		parent::__construct(self::MESSAGE_AUTH_SSO_FAILED, self::CODE_AUTH_SSO_FAILED);
	}
}
