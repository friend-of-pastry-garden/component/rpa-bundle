<?php
namespace FOPG\Component\RpaBundle\Exception\Scraper;

use FOPG\Component\RpaBundle\Contracts\Scraper\ExceptionInterface;

class ClickFailedException extends \Exception implements ExceptionInterface
{
	public function __construct(string $message)
	{
		parent::__construct("Click failed due of :".$message, self::CODE_EXECUTE_FAILED);
	}
}
