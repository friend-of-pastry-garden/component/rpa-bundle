<?php
namespace FOPG\Component\RpaBundle\Exception\Scraper;

use FOPG\Component\RpaBundle\Contracts\Scraper\ExceptionInterface;

class NoFormFoundException extends \Exception implements ExceptionInterface
{
	public function __construct(string $label)
	{
		parent::__construct("No form found with label '$label'", self::CODE_NO_FORM_FOUND);
	}
}
