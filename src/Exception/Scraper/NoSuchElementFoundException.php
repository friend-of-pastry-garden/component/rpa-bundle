<?php
namespace FOPG\Component\RpaBundle\Exception\Scraper;

use FOPG\Component\RpaBundle\Contracts\Scraper\ExceptionInterface;

class NoSuchElementFoundException extends \Exception implements ExceptionInterface
{
	public function __construct(string $classname)
	{
		parent::__construct("element $classname not found!", self::CODE_NO_SUCH_ELEMENT_FOUND);
	}
}
