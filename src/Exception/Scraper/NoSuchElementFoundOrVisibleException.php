<?php
namespace FOPG\Component\RpaBundle\Exception\Scraper;

use FOPG\Component\RpaBundle\Contracts\Scraper\ExceptionInterface;

class NoSuchElementFoundOrVisibleException extends \Exception implements ExceptionInterface
{
	public function __construct(string $classname)
	{
		parent::__construct("element $classname is not found and/or not visible!", self::CODE_NO_SUCH_ELEMENT_FOUND_OR_VISIBLE);
	}
}
