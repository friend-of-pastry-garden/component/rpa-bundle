<?php
namespace FOPG\Component\RpaBundle\Scraper;

use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;
use Facebook\WebDriver\WebDriverCapabilities;
use FOPG\Component\RpaBundle\Contracts\Scraper\ClientInterface;
use Symfony\Component\DomCrawler\Crawler as DomCrawler;
use Symfony\Component\DomCrawler\Form as DomForm;
use Symfony\Component\Panther\Client as PantherClient;

class ChromeClient extends Client
{
  public function __construct() {
    parent::__construct(PantherClient::createChromeClient());
  }

  final public function quit(): void {
    $this->getInstance()->quit();
  }

  final public function workWithJS(): bool {
    return true;
  }
}
