<?php
namespace FOPG\Component\RpaBundle\Scraper;

use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverOptions;
use FOPG\Component\RpaBundle\Contracts\Scraper\ClientInterface;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DomCrawler\Crawler as DomCrawler;
use Symfony\Component\DomCrawler\Form as DomForm;
use Symfony\Component\Panther\Cookie\CookieJar;

abstract class Client implements ClientInterface
{
  private mixed $_instance=null;

  public function __construct(mixed $instance) {
    $this->_instance = $instance;
  }

  public function getPersist(): bool
  {
    if (method_exists($this->_instance, 'getPersist'))
      return $this->_instance->getPersist();
    return false;
  }
  public function setPersist(bool $persist): self
  {
    if (method_exists($this->_instance, 'setPersist'))
      $this->_instance->setPersist($persist);

    return $this;
  }

  public function __destruct() {
    unset($this->_instance);
  }

  public function loadFromSessionID(string $sessionID,string $serverUrl): self {
    $driver = RemoteWebDriver::createBySessionID($sessionID, $serverUrl);

    $this
      ->getInstance()
      ->setWebDriver($driver)
    ;
    return $this;
  }

  public function getSessionID(): string {

    return $this
      ->getInstance()
      ->getWebDriver()
      ->getSessionID()
    ;

  }
  public function getCookies(): array {
    $cookieJar = $this->getInstance()->getCookieJar();
    $tmp = [];
    foreach($cookieJar->all() as $cookie)
      $tmp[]=serialize($cookie);
    return $tmp;
  }

  public function getInstance(): mixed {
    return $this->_instance;
  }

  final public function submit(
    DomForm $domForm,
    array $data
  ): ?DomCrawler {
    return $this->getInstance()->submit($domForm, $data);
  }

  final public function execute(
    string $url,
    string $method=Request::METHOD_GET,
    array $queryParameters=[]
  ): ?DomCrawler {
    /** @var DomCrawler $crawler */
    $crawler = $this->getInstance()->request($method, $url, $queryParameters);
    return $crawler;
  }

  /**
   * @var int
   *
   * @return int
   */
  public function getStatusCode(): int {
    return $this
      ->getResponse()
      ->getStatusCode()
    ;
  }

  public function getCrawler(): DomCrawler
  {
    return $this->getInstance()->getCrawler();
  }

  /**
   * @var Response
   *
   * @return Response
   */
  public function getResponse(): Response {
    return $this->_instance->getInternalResponse();
  }
}
