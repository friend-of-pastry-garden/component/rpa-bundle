<?php
namespace FOPG\Component\RpaBundle\Scraper;

use Goutte\Client as GClient;
use FOPG\Component\RpaBundle\Contracts\Scraper\ClientInterface;
use Symfony\Component\DomCrawler\Crawler as DomCrawler;
use Symfony\Component\DomCrawler\Form as DomForm;
use Symfony\Component\HttpClient\HttpClient;

class GoutteClient extends Client
{
  public function __construct() {
    $no_proxy = exec('echo $no_proxy');
    $proxy    = exec('echo $http_proxy');
    /** @var GoutteClient $goutteClient */
    $goutteClient = new GClient(HttpClient::create([
      'no_proxy' => $no_proxy,
      'proxy'   => $proxy,
      "verify_peer"=>false,
      "verify_host"=>false
    ]));
    parent::__construct($goutteClient);
  }

  final public function quit(): void { }

  final public function workWithJS(): bool {
    return false;
  }
}
