<?php

namespace FOPG\Component\RpaBundle\Scraper;

use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Remote\RemoteWebElement;
use FOPG\Component\RpaBundle\Contracts\Scraper\ClientInterface;
use FOPG\Component\RpaBundle\Contracts\Scraper\ScraperInterface;
use FOPG\Component\RpaBundle\Exception\Scraper\AuthSsoFailedException;
use FOPG\Component\RpaBundle\Exception\Scraper\ClickFailedException;
use FOPG\Component\RpaBundle\Exception\Scraper\ExecuteFailedException;
use FOPG\Component\RpaBundle\Exception\Scraper\NoFormFoundException;
use FOPG\Component\RpaBundle\Exception\Scraper\NoSuchElementFoundException;
use FOPG\Component\RpaBundle\Exception\Scraper\NoSuchElementFoundOrVisibleException;
use FOPG\Component\UtilsBundle\SimpleHtml\SimpleHtml;
use FOPG\Component\UtilsBundle\SimpleHtml\SimpleHtmlDom;
use FOPG\Component\UtilsBundle\Contracts\UriInterface;
use FOPG\Component\UtilsBundle\Uri\Uri;
use Symfony\Component\DomCrawler\Form as DomForm;
use Symfony\Component\DomCrawler\Crawler as DomCrawler;
use Symfony\Component\HttpFoundation\Request;

class Scraper implements ScraperInterface
{
  private ?ClientInterface $_client=null;
  private ?UriInterface $_uri=null;
  private array $_constraints=[];
  private array $_sso=[];
  private ?ScraperErrorCollection $_errorCollection=null;

  public function getErrors(): ?ScraperErrorCollection {
    return $this->_errorCollection;
  }

  public function isValid(): bool {
    return (!empty($this->_errorCollection) && ($this->_errorCollection->count() === 0));
  }

  public function clearErrors(): self {
    if(!empty($this->_errorCollection))
      $this->_errorCollection->clear();
    return $this;
  }

  public function addError(\Throwable $e): self {
    if(!empty($this->_errorCollection))
      $this->_errorCollection->addError($e);
    return $this;
  }

  /**
   * @param ClientInterface $client
   * @param bool $persist Le client doit t'il conserver les sessions actives
   */
  public function __construct(ClientInterface $client, bool $persist=false) {
    $this->_client = $client;
    $this->_client->setPersist($persist);
    $this->_errorCollection = new ScraperErrorCollection();
    $this->_uri = new Uri(["http","https"], [80,443]);
  }

  public function getCookies(): mixed {
    return $this->_client->getCookies();
  }

  public function getSessionID(): ?string {
    return $this->_client->getSessionID();
  }

  public function hasSso(): bool {
    return (count($this->_sso)>0);
  }

  public function setSso(array $sso): self {
    $this->_sso = $sso;
    return $this;
  }

  public function addConstraint(ScraperConstraint $constraint): self {
    $this->_constraints[] = $constraint;
    return $this;
  }

  public function writeInForm(string $selector, array $data): ?DomForm {
    /** @var DomCrawler $crawler */
    $crawler = $this
      ->_client
      ->getInstance()
      ->getCrawler()
    ;
    $form = $crawler
      ->filter($selector)
      ->form($data)
    ;
    return $form;
  }

  public function execJS(array $commands): mixed {
    array_walk($commands, function($line, $index)use(&$commands) {
      $commands[$index]=str_replace(['\\'], ['\\\\'], $line);
    });
    return $this
      ->_client
      ->getInstance()
      ->executeScript(implode("\r\n",$commands))
    ;
  }

  public function click(
    string $selector,
    string $strategy = self::STRATEGY_DOM_CRAWLER
  ): ?DomCrawler {
    $this->clearErrors();
    /** @var ?DomCrawler $crawler */
    $crawler = null;
    if(self::STRATEGY_JS === $strategy && in_array('executeScript', get_class_methods($this->_client->getInstance())))
    {
      $selector = str_replace("'","\'", $selector);
      $newSelector = preg_replace("/^[#]/","",$selector);
      if($newSelector !== $selector)
      {
        $this->_client->getInstance()->executeScript("document.getElementById('$newSelector').click()");
        $crawler = $this->_client->getInstance()->getCrawler();
      }
      else
      {
        $this->_client->getInstance()->executeScript("document.querySelector('$newSelector').click()");
        sleep(1);
        $crawler = $this->_client->getInstance()->refreshCrawler();
      }
    }
    elseif(self::STRATEGY_DOM_CRAWLER === $strategy)
    {
      $crawler = $this->_client->getCrawler();
      $nodeList = $crawler->filter($selector);
      $node     = count($nodeList) ? $nodeList->first() : null;
      if($node)
        $node->click();
      $crawler = $this->_client->getCrawler();
    }
    $crawler = $this->postExecute($crawler);

    return $crawler;
  }

  public function prepare(string $url): UriInterface {
    $this->_uri->sanitize($url);
    return $this->_uri;
  }

  public function getUri(): UriInterface {
    return $this->_uri;
  }

  private function postExecute(?DomCrawler $crawler): ?DomCrawler {
    $constraints = $this->_constraints;
    $classname   = null;
    foreach($constraints as $constraint)
    {
      switch($constraint->getAction())
      {
        case ScraperConstraint::ACTION_AWAITING:
          sleep($constraint->getTimeout());
          break;
        case ScraperConstraint::ACTION_CHECK_CLASS:
          $classname = ".".$constraint->getParam();
        case ScraperConstraint::ACTION_CHECK_ID:
          $classname = $classname ?: "#".$constraint->getParam();
        case ScraperConstraint::ACTION_CHECK_SELECTOR:
          $classname = $classname ?: $constraint->getParam();
          $client = $this->_client->getInstance();
          $timeout = $constraint->getTimeout();
          if(
              $constraint->getState() === ScraperConstraint::STATE_VISIBLE &&
              in_array("waitForVisibility",get_class_methods($client))
          ) {
            try { $crawler = $client->waitForVisibility($classname, $timeout); }
            catch(\Exception $e) { $this->addError(new NoSuchElementFoundOrVisibleException($classname)); }
          }
          elseif(
              $constraint->getState() === ScraperConstraint::STATE_PRESENT &&
              in_array("waitFor",get_class_methods($client))
          ) {
            try { $crawler = $client->waitFor($classname, $timeout); }
            catch(\Exception $e) { $this->addError(new NoSuchElementFoundException($classname)); }
          }
          break;
        default:
          break;
      }
      unset($constraint);
    }

    $this->_constraints = [];

    return $crawler;
  }

  public function getClient(): ClientInterface {
    return $this->_client;
  }

  public function getCrawler(): ?DomCrawler {
    return $this->_client->getCrawler();
  }

  public function execute(string $method=Request::METHOD_GET): ?DomCrawler {
    $this->clearErrors();
    $url = (string)$this->_uri;
    $queryParameters=[];
    try {
      $crawler = $this
        ->_client
        ->execute($url, $method, $queryParameters)
      ;
    }
    catch(\Exception $e) {
      $this
        ->addError(new ExecuteFailedException($e->getMessage()))
      ;
      return null;
    }
    $ssoLink = (string)$this->_client->getCrawler()->getUri();
    // Are we redirect to SSO ?
    if(
      !empty($this->_sso[Scraper::SSO_URL_REGEXP]) &&
      preg_match($this->_sso[Scraper::SSO_URL_REGEXP], $ssoLink)
    ) {
      $this->submit([
        Scraper::FORM_LABEL => $this->_sso[Scraper::SSO_LABEL],
        Scraper::FORM_PARAMETERS => $this->_sso[Scraper::SSO_PARAMS],
        Scraper::FORM_OPTIONS => [
          Scraper::FORM_DISABLE_PRE_EXECUTE => true,
        ]
      ]);
      try {
        $crawler = $this
          ->_client
          ->execute($url, $method, $queryParameters)
        ;
      }
      catch(\Exception $e) {
        $this
          ->addError(new ExecuteFailedException($e->getMessage()))
        ;
        return null;
      }
      /** @var UriInterface $uri */
      $uri = $this
        ->_client
        ->getCrawler()
        ->getUri()
      ;
      $ssoLink = (string)$uri;
      if(preg_match($this->_sso[Scraper::SSO_URL_REGEXP], $ssoLink)) {
        $this->addError(new AuthSsoFailedException());
        return null;
      }
    }

    $crawler = $this->postExecute($crawler);

    return $crawler;
  }

  public function getCurrentURL(): ?string {
    if(true === $this->getClient()->workWithJS())
      return $this->execJs(["return location.href;"]);
    else
      return (string)($this->_client->getCrawler()->getUri());
    ;
  }

  public function submit(array $form=[]): ?DomCrawler {
    $this->clearErrors();
    $disablePreExecute = !empty($form[self::FORM_OPTIONS]) &&
      !empty($form[self::FORM_OPTIONS][self::FORM_DISABLE_PRE_EXECUTE]) &&
      (true === $form[self::FORM_OPTIONS][self::FORM_DISABLE_PRE_EXECUTE])
    ;
    if(false === $disablePreExecute)
      $crawler = $this->execute();
    else
      $crawler = $this->_client->getInstance()->getCrawler();

    /** @var DomForm $domForm */
    $domForm = null;
    if(!empty($form[self::FORM_LABEL]) || !empty($form[self::FORM_NAME]))
    {
      try {
        if(!empty($form[self::FORM_LABEL]))
          $domForm = $crawler ? $crawler->selectButton($form[self::FORM_LABEL])->form() : null;
        elseif(!empty($form[self::FORM_NAME])) {
          $opname = "form[name='".$form[self::FORM_NAME]."']";
          $domForm = $crawler ? $crawler->filter($opname)->form() : null;
          unset($opname);
        }
        if(!$domForm)
        {
          $this->addError(new NoFormFoundException($form[self::FORM_LABEL]));
          return null;
        }
      }
      catch(\Exception $e) {
        $this->addError(new NoFormFoundException($form[self::FORM_LABEL] ?? "none"));
        return null;
      }
    }

    /** @var array $data */
    $data = !empty($form[self::FORM_PARAMETERS]) ? $form[self::FORM_PARAMETERS] : [];
    $crawler = $this->_client->submit($domForm, $data);

    $crawler = $this->postExecute($crawler);

    return $crawler;
  }

  public function getStatusCode(): int {
    return $this->_client->getStatusCode();
  }

  public function getHtml(string $selector=null): ?string {
    return $this->export($selector, 'getHtml');
  }

  public function getText(string $selector=null): ?string {
    return $this->export($selector, 'getText');
  }

  public function getSimpleHtml(string $selector=null): ?SimpleHtmlDom {
    $html = $this->getHtml($selector);
    if(!$html)
      return null;
    /** @var SimpleHtmlDom $dom */
    $dom = SimpleHtml::str_get_html($html)->getContainer();
    return $dom;
  }

  /**
   * Retrieve the HTML after execute
   *
   * @return string
   */
  public function export(string $selector=null, string $operator): ?string {
    $crawler = $this->_client->getCrawler();
    if(null !== $crawler) {
      $html = "";
      if(null !== $selector) {
        $fullHtml = $crawler->html();
        $simpleHtml = new SimpleHtmlDom($fullHtml);
        $nodeList = $simpleHtml->findAll($selector);
        foreach($nodeList as $nodeItem) {
          $html.=$nodeItem->$operator();
        }
        unset($simpleHtml);
      }
      else
        $html = $crawler->html();
      return $html;
    }

    return null;
  }

  /**
  * waiting during native click JS
  *
  * @param string $selector
  * @param int $retry
  * @param int $timeout
  * @return bool
  */
 public function awaiting(string $selector, int $retry=3,int $timeout=1): void {
   $cpt = 0;
   do {
     if($cpt) { sleep($timeout); }

     $this
       ->getClient()
       ->getInstance()
       ->refreshCrawler()
     ;
     $count = count($this->getSimpleHtml()->findAll($selector));
     $check = ($count>0);
     $cpt++;
     if($cpt>=$retry)
       return;
   }while(false === $check);
 }
}
