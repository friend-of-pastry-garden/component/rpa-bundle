<?php
namespace FOPG\Component\RpaBundle\Scraper;

use FOPG\Component\RpaBundle\Contracts\Scraper\ScraperConstraintInterface;

class ScraperConstraint implements ScraperConstraintInterface
{
  private string $_action;
  private int $_timeout = 10;
  private ?string $_param;
  private string $_state;

  public function __construct(string $action = self::ACTION_AWAITING, string $state=self::STATE_NONE) {
    $this->_action = $action;
    $this->_state = $state;
  }

  public function getAction(): string {
    return $this->_action;
  }

  public function getState(): string {
    return $this->_state;
  }

  public function getParam(): ?string {
    return $this->_param;
  }

  public function setParam(?string $param): self {
    $this->_param = $param;
    return $this;
  }

  public function getTimeout(): int {
    return $this->_timeout;
  }

  public function setTimeout(int $timeout): self {
    $this->_timeout = $timeout;
    return $this;
  }
}
