<?php
namespace FOPG\Component\RpaBundle\Scraper;

class ScraperError
{
  private \Throwable $_e;

  public function __construct(\Throwable $e) {
    $this->_e = $e;
  }

  public function getMessage(): string {
    return $this->_e->getMessage();
  }

  public function getCode(): int {
    return $this->_e->getCode();
  }
}
