<?php
namespace FOPG\Component\RpaBundle\Scraper;

//use Doctrine\Common\Collections\ArrayCollection;

class ScraperErrorCollection implements \Countable, \IteratorAggregate
{
  private array $_items = [];

  public function count(): int {
    return count($this->_items);
  }

  public function getIterator(): \Traversable {
    return new \ArrayIterator($this->_items);
  }

  public function add(ScraperError $e): self {
    $this->_items[]=$e;
    return $this;
  }

  public function clear(): void {
    foreach($this->_items as $item)
      unset($item);
    $this->_items = [];
  }

  /**
   * @param int $code
   * @param string $message
   * @return self
   */
  public function addError(\Throwable $e): self
  {
    $scraperError = new ScraperError($e);
    $this->add($scraperError);
    return $this;
  }


  /**
   * @param int $errorCode
   * @return bool
   */
  public function hasErrorCode(int $errorCode): bool
  {
    $check = false;
    foreach($this->_items as $scraperError)
      if($scraperError->getCode() === $errorCode)
        $check = true;
    return $check;
  }
}
