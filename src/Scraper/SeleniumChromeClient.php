<?php
namespace FOPG\Component\RpaBundle\Scraper;

use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;
use Facebook\WebDriver\WebDriverCapabilities;
use FOPG\Component\RpaBundle\Contracts\Scraper\ClientInterface;
use Symfony\Component\DomCrawler\Crawler as DomCrawler;
use Symfony\Component\DomCrawler\Form as DomForm;
use FOPG\Component\RpaBundle\Scraper\PantherClient as PantherClient;

class SeleniumChromeClient extends Client
{
  public function __construct(string $serverUrl, string $proxy="") {
    /** @var ChromeOptions $options */
    $options = new ChromeOptions();
    $args = ['--ignore-certificate-errors'];

    $isCassiopeeProxy = !empty($proxy) && (strlen($proxy) > 0);

    if(true === $isCassiopeeProxy) {
      array_push($args,  '--host-rules=MAP * '.$proxy);
    }

    $options->addArguments($args);

    $capabilities = new DesiredCapabilities([
        WebDriverCapabilityType::PLATFORM => 'ANY',
        'browserName' => 'chrome',
        WebDriverCapabilityType::ACCEPT_SSL_CERTS => true,
        ChromeOptions::CAPABILITY => ChromeOptions::CAPABILITY_W3C]);
    $capabilities->setCapability(ChromeOptions::CAPABILITY, $options);

    parent::__construct(PantherClient::createSeleniumClient($serverUrl, $capabilities));
  }

  final public function quit(): void {
    $this->getInstance()->quit();
  }

  final public function workWithJS(): bool {
    return true;
  }
}
