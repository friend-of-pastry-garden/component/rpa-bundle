<?php
namespace FOPG\Component\RpaBundle\Scraper;

use Facebook\WebDriver\Firefox\FirefoxOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;
use Facebook\WebDriver\WebDriverCapabilities;
use FOPG\Component\RpaBundle\Contracts\Scraper\ClientInterface;
use Symfony\Component\DomCrawler\Crawler as DomCrawler;
use Symfony\Component\DomCrawler\Form as DomForm;
use Symfony\Component\Panther\Client as PantherClient;

class SeleniumFirefoxClient extends Client
{
  final public function workWithJS(): bool {
    return true;
  }

  public function __construct(string $serverUrl) {
    $options = new FirefoxOptions();
    $options->addArguments([
      '--ignore-certificate-errors',
    ]);

    $capabilities = new DesiredCapabilities([
        WebDriverCapabilityType::PLATFORM => 'ANY',
        'browserName' => 'firefox',
        WebDriverCapabilityType::ACCEPT_SSL_CERTS => true,
    ]);
    $capabilities->setCapability(FirefoxOptions::CAPABILITY, $options);

    parent::__construct(PantherClient::createSeleniumClient($serverUrl, $capabilities));
  }

  final public function quit(): void {
    $this->getInstance()->quit();
  }
}
