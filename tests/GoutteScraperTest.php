<?php

namespace FOPG\Component\RpaBundle\Tests;

use FOPG\Component\UtilsBundle\Test\TestCase;
use FOPG\Component\UtilsBundle\Test\TestGiven;
use FOPG\Component\UtilsBundle\Env\Env;
use FOPG\Component\RpaBundle\Scraper\GoutteClient;
use FOPG\Component\RpaBundle\Scraper\Scraper;
use FOPG\Component\RpaBundle\Scraper\ScraperConstraint;
class GoutteScraperTest extends TestCase
{
    const SECTION_HEADER = '[Scraper:GoutteScraper]';

    public function testParcoursSimple(): void
    {
        /** @var ?string $url */
        $url = Env::get("TEST__DEFAULT_WEBSITE");
        /** @var ?string $tag */
        $tag = Env::get("TEST__DEFAULT_TAG");
        /** @var ?string $val */
        $val = Env::get("TEST__DEFAULT_TAG_VALUE");

        $this->section(self::SECTION_HEADER.' Interrogation de site web');
        /** @var GoutteClient $goutteClient */
        $goutteClient = new GoutteClient();
        /** @var Scraper $scraper */
        $scraper = new Scraper($goutteClient);

        if(null === $url) {
          $this->iteration('L\'url du site web à tester (variable d\'environnement TEST__DEFAULT_WEBSITE) n\'est pas configuré. Le test est ignoré');
          return;
        }
        if(null === $tag) {
          $this->iteration('La balise du site web à tester (variable d\'environnement TEST__DEFAULT_TAG) n\'est pas configurée. Le test est ignoré');
          return;
        }
        if(null === $val) {
          $this->iteration('Le texte de la balise du site web à tester (variable d\'environnement TEST__DEFAULT_TAG_VALUE) n\'est pas configurée. Le test est ignoré');
          return;
        }

        $this
          ->given(
            description: "Contrôle d'accès à une page",
            scraper: $scraper,
            url: $url,
            tag: $tag,
            val: $val
          )
          ->when(
            description: "Je tente d'accéder à la page d'accueil en mettant une attente de 3 secondes avant récupération",
            callback: function(Scraper $scraper,string $url) {
              $scraper->prepare($url);

              $constraint = new ScraperConstraint(ScraperConstraint::ACTION_AWAITING);
              $constraint->setTimeout(3);
              $scraper->addConstraint($constraint);

              $scraper->execute();
            }
          )
          ->then(
            description: "Le statut du scraper doit être valide",
            callback: function(Scraper $scraper) {
              return $scraper->isValid();
            },
            result: true,
            onFail: function(Scraper $scraper, TestGiven $whoami) {
              foreach($scraper->getErrors() as $err)
                $whoami->addError($err->getMessage(), $err->getCode());
            }
          )
          ->andThen(
            description: "Je peux récupérer le contenu de la balise $tag de l'HTML",
            callback: function(Scraper $scraper, string $tag) {
              return $scraper->getText($tag);
            },
            result: $val,
            onFail: function(Scraper $scraper, string $tag, string $val, TestGiven $whoami) {
              $whoami->addError("'".$scraper->getText($tag)."' trouvé, '$val' attendu",100);
            }
          )
        ;
    }
}
