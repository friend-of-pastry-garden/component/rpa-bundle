<?php

namespace FOPG\Component\RpaBundle\Tests;

use FOPG\Component\UtilsBundle\Test\TestCase;
use FOPG\Component\UtilsBundle\Test\TestGiven;
use FOPG\Component\UtilsBundle\Env\Env;
use FOPG\Component\RpaBundle\Scraper\SeleniumChromeClient;
use FOPG\Component\RpaBundle\Scraper\Scraper;
use FOPG\Component\RpaBundle\Scraper\ScraperConstraint;

class SeleniumChromeScraperTest extends TestCase
{
    const SECTION_HEADER = '[Scraper:SeleniumChromeScraper]';

    public function testParcoursSso(): void
    {
      /** @var ?string $seleniumUrl */
      $seleniumUrl = Env::get("TEST__DEFAULT_SELENIUM_CHROME_SERVER_URL");
      /** @var ?string $url */
      $url = Env::get("TEST__DEFAULT_WEBSITE_WITH_SSO");
      /** @var ?string $btnSsoLabel */
      $btnSsoLabel = Env::get("TEST__DEFAULT_SSO_LABEL");
      /** @var ?array $paramsSso */
      $paramsSso = Env::get("TEST__DEFAULT_SSO_PARAMS");
      /** @var ?string $ssoUrlRexp */
      $ssoUrlRexp = Env::get("TEST__DEFAULT_SSO_URL_REGEXP");
      /** @var ?string $ssoTag */
      $ssoTag = Env::get('TEST__DEFAULT_SSO_TAG');
      /** @var ?string $ssoTagValue */
      $ssoTagValue = Env::get('TEST__DEFAULT_SSO_TAG_VALUE');

      $this->section(self::SECTION_HEADER.' Interrogation de site web utilisant un SSO');
      /** @var SeleniumChromeClient $seleniumChromeClient */
      $seleniumChromeClient = new SeleniumChromeClient($seleniumUrl);
      /** @var Scraper $scraper */
      $scraper = new Scraper($seleniumChromeClient);

      if(null === $seleniumUrl) {
        $this->iteration('L\'adresse du serveur selenium (variable d\'environnement TEST__DEFAULT_SELENIUM_CHROME_SERVER_URL) n\'est pas configuré. Le test est ignoré');
        return;
      }
      if(null === $url) {
        $this->iteration('L\'url du site web à tester (variable d\'environnement TEST__DEFAULT_WEBSITE_WITH_SSO) n\'est pas configuré. Le test est ignoré');
        return;
      }
      if(null === $btnSsoLabel) {
        $this->iteration('Le libellé du bouton de soumission de la page SSO (variable d\'environnement TEST__DEFAULT_SSO_LABEL) n\'est pas configuré. Le test est ignoré');
        return;
      }
      if(null === $paramsSso) {
        $this->iteration('Le tableau de paramètrage pour la connexion SSO (variable d\'environnement TEST__DEFAULT_SSO_PARAMS) n\'est pas configuré. Le test est ignoré');
        return;
      }
      if(null === $ssoUrlRexp) {
        $this->iteration('L\'expression régulière de vérification de redirection sur la page SSO (variable d\'environnement TEST__DEFAULT_SSO_URL_REGEXP) n\'est pas configuré. Le test est ignoré');
        return;
      }
      /** @todo texte à terminer */
      if(null === $ssoTag) {
        $this->iteration('La balise de contrôle de bonne redirection après passage par l\'authentification SSO (variable d\'environnement TEST__DEFAULT_SSO_TAG) n\'est pas configuré. Le test est ignoré');
        return;
      }
      if(null === $ssoTagValue) {
        $this->iteration('Le texte contenu dans la balise de contrôle de bonne redirection après passage par l\'authentification SSO (variable d\'environnement TEST__DEFAULT_SSO_TAG_VALUE) n\'est pas configuré. Le test est ignoré');
        return;
      }
      $this
        ->given(
          description: 'Connexion à un site utilisant une redirection SSO',
          scraper: $scraper,
          url: $url,
          btnSsoLabel: $btnSsoLabel,
          paramsSso: $paramsSso,
          ssoUrlRexp: $ssoUrlRexp,
          ssoTag: $ssoTag,
          ssotagValue: $ssoTagValue
        )
        ->when(
          description: 'Je configure le scraper avec les informations pour se connecter au SSO',
          callback: function(
            Scraper $scraper,
            string $url,
            string $btnSsoLabel,
            array $paramsSso,
            string $ssoUrlRexp
          ) {
            $scraper->prepare($url);
            $scraper->setSso([
              Scraper::SSO_LABEL => $btnSsoLabel,
              Scraper::SSO_PARAMS => $paramsSso,
              Scraper::SSO_URL_REGEXP => $ssoUrlRexp,
            ]);
          }
        )
        ->andWhen(
          description: 'Je tente de me connecter à la page protégée par la redirection SSO',
          callback: function(
            Scraper $scraper,
            string $ssoTag
          ) {
            $constraint = new ScraperConstraint(ScraperConstraint::ACTION_CHECK_SELECTOR, ScraperConstraint::STATE_PRESENT);
            $constraint->setParam($ssoTag);
            $constraint->setTimeout(30);
            $scraper->addConstraint($constraint);
            $scraper->execute();
          }
        )
        ->then(
          description: "Le scraper doit être valide",
          callback: function(Scraper $scraper) {
            return $scraper->isValid();
          },
          result: true,
          onFail: function(Scraper $scraper, TestGiven $whoami) {
            foreach($scraper->getErrors() as $err)
              $whoami->addError($err->getMessage(), $err->getCode());
          }
        )
        ->andThen(
          description: "La valeur de la balise de la page courante après connexion doit être retrouvé",
          callback: function(Scraper $scraper) {
            return $scraper->getText('title');
          },
          result: $ssoTagValue,
          onFail: function(Scraper $scraper, ?string $ssoTagValue, TestGiven $whoami) {
            $whoami->addError("'".$scraper->getText('title')."' trouvé, '$ssoTagValue' attendu",100);
          }
        )
      ;
    }

    public function testParcoursSimple(): void
    {
        /** @var ?string $seleniumUrl */
        $seleniumUrl = Env::get("TEST__DEFAULT_SELENIUM_CHROME_SERVER_URL");
        /** @var ?string $url */
        $url = Env::get("TEST__DEFAULT_WEBSITE");
        /** @var ?string $tag */
        $tag = Env::get("TEST__DEFAULT_TAG");
        /** @var ?string $val */
        $val = Env::get("TEST__DEFAULT_TAG_VALUE");

        $this->section(self::SECTION_HEADER.' Interrogation de site web');
        /** @var SeleniumChromeClient $seleniumChromeClient */
        $seleniumChromeClient = new SeleniumChromeClient($seleniumUrl);
        /** @var Scraper $scraper */
        $scraper = new Scraper($seleniumChromeClient);

        if(null === $seleniumUrl) {
          $this->iteration('L\'adresse du serveur selenium (variable d\'environnement TEST__DEFAULT_SELENIUM_CHROME_SERVER_URL) n\'est pas configuré. Le test est ignoré');
          return;
        }
        if(null === $url) {
          $this->iteration('L\'url du site web à tester (variable d\'environnement TEST__DEFAULT_WEBSITE) n\'est pas configuré. Le test est ignoré');
          return;
        }
        if(null === $tag) {
          $this->iteration('La balise du site web à tester (variable d\'environnement TEST__DEFAULT_TAG) n\'est pas configurée. Le test est ignoré');
          return;
        }
        if(null === $val) {
          $this->iteration('Le texte de la balise du site web à tester (variable d\'environnement TEST__DEFAULT_TAG_VALUE) n\'est pas configurée. Le test est ignoré');
          return;
        }

        $this
          ->given(
            description: "Accès simple à une page",
            scraper: $scraper,
            url: $url,
            tag: $tag,
            val: $val
          )
          ->when(
            description: "Je tente d'accéder à la page d'accueil en mettant une attente de 10 secondes avant récupération",
            callback: function(Scraper $scraper,string $url) {
              $scraper->prepare($url);

              $constraint = new ScraperConstraint(ScraperConstraint::ACTION_AWAITING);
              $constraint->setTimeout(10);
              $scraper->addConstraint($constraint);

              $scraper->execute();
            }
          )
          ->then(
            description: "Le statut du scraper doit être valide",
            callback: function(Scraper $scraper) {
              return $scraper->isValid();
            },
            result: true,
            onFail: function(Scraper $scraper, TestGiven $whoami) {
              foreach($scraper->getErrors() as $err)
                $whoami->addError($err->getMessage(), $err->getCode());
            }
          )
          ->andThen(
            description: "Je peux récupérer le contenu de la balise $tag de l'HTML",
            callback: function(Scraper $scraper, string $tag, string $val) {
              return $scraper->getText($tag);
            },
            result: $val,
            onFail: function(Scraper $scraper, string $tag, string $val, TestGiven $whoami) {
              $whoami->addError("'".$scraper->getText($tag)."' trouvé, '$val' attendu",100);
            }
          )
          ->andThen(
            description: "J'applique la méthode 'awaiting' d'attente de chargement natif",
            callback: function(Scraper $scraper, string $tag) {
              $scraper->awaiting(selector: $tag, retry: 3, timeout: 1);
              return true;
            },
            result: true
          )
        ;
    }
}
